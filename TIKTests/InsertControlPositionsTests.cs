﻿using System;
using System.Collections;
using NUnit.Framework;
using TIKLab;

namespace TIKTests
{
    [TestFixture]
    public class InsertControlPositionsTests
    {
        [Test]
        public void TestMethod1()
        {
            int maskSize = 32;
            var generator = new BitMaskGenerator(maskSize);
            var mask = generator.GenerateBitMask(0, 1);
            var encoder = new HammingEncoder();
            encoder.SetFields(15, 4, 32);
            var editedMask = encoder.InsertControlPositions(mask);
            var example = new bool[]
            {
                false, false, true, false,  false, true, false, false,
                true, false, true, false,   true, false, true, false,
                false, true, false, true,   false, false, false, false,
                false, false, false, false, false, false, false, false
            };
            var exampleArray = new BitArray(example);

            Assert.AreEqual(exampleArray, editedMask, $"Not equal: {editedMask.ToStringCustom()}, {exampleArray.ToStringCustom()}");
            Assert.Pass($"Sorta passed: {editedMask.ToStringCustom()}");
        }
    }
}
