﻿using System;
using System.Collections;
using NUnit.Framework;
using TIKLab;

namespace TIKTests
{
    [TestFixture]
    public class BitMaskGeneratorTest
    {
        [Test]
        public void TestMethod1()
        {
            int maskSize = 32;
            var maskExample = new BitArray(maskSize);
            for (int i = 0; i < maskSize; i+=2)
                maskExample[i] = true;
            var generator = new BitMaskGenerator(maskSize);
            var mask = generator.GenerateBitMask(0, 1);
            Assert.AreEqual(mask, maskExample, $"Masks werent equal. Example:{maskExample.ToStringCustom()}, actual: {mask.ToStringCustom()}");
            Assert.Pass($"Masks were equal: {maskExample.ToStringCustom()}");
        }

        [Test]
        public void TestMethod2()
        {
            int maskSize = 32;
            var maskExample = new BitArray(maskSize);
            for (int i = 1; i < maskSize; i += 4)
                maskExample[i] = true;
            for (int i = 2; i < maskSize; i += 4)
                maskExample[i] = true;
            var generator = new BitMaskGenerator(maskSize);
            var mask = generator.GenerateBitMask(1, 2);
            Assert.AreEqual(mask, maskExample, $"Masks werent equal. Example:{maskExample.ToStringCustom()}, actual: {mask.ToStringCustom()}");
            Assert.Pass($"Masks were equal: {maskExample.ToStringCustom()}");
        }

        [Test]
        public void TestMethod3()
        {
            int maskSize = 32;
            var maskExample = new BitArray(maskSize);
            for (int i = 3; i < maskSize; i += 8)
                maskExample[i] = true;
            for (int i = 4; i < maskSize; i += 8)
                maskExample[i] = true;
            for (int i = 5; i < maskSize; i += 8)
                maskExample[i] = true;
            for (int i = 6; i < maskSize; i += 8)
                maskExample[i] = true;
            var generator = new BitMaskGenerator(maskSize);
            var mask = generator.GenerateBitMask(3, 4);
            Assert.AreEqual(mask, maskExample, $"Masks werent equal. Example:{maskExample.ToStringCustom()}, actual: {mask.ToStringCustom()}");
            Assert.Pass($"Masks were equal: {maskExample.ToStringCustom()}");
        }
    }
}
