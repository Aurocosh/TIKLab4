﻿using System.Collections;

namespace TIKLab
{
    public struct Code
    {
        public Code(char symbol, BitArray codeBits)
        {
            Symbol = symbol;
            CodeBits = codeBits;
        }

        public char Symbol;
        public BitArray CodeBits;
    }
}
