﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIKLab
{
    public static class Helper
    {
        public static int IntPower(int x, int power)
        {
            return (power == 0) ? x :
                ((power & 0x1) == 0 ? x : 1) *
                    IntPower(x, power >> 1);
        }

        public static byte[] BitArrayToByteArray(this BitArray bits)
        {
            byte[] ret = new byte[(bits.Length - 1) / 8 + 1];
            bits.CopyTo(ret, 0);
            return ret;
        }

        public static int ToIntFromBitArray(this BitArray bitArray)
        {
            if (bitArray.Length > 32)
                throw new ArgumentException("Argument length shall be at most 32 bits.");

            var array = new int[1];
            bitArray.CopyTo(array, 0);
            return array[0];
        }

        public static string ToStringLimited(this BitArray bitArray, int limit)
        {
            var builder = new StringBuilder();
            for (int i = 0; i < limit; i++)
                builder.Append(bitArray[i] ? 1 : 0);
            return builder.ToString();
        }

        public static string ToStringCustom(this BitArray bitArray)
        {
            var builder = new StringBuilder();
            var limit = bitArray.Length;
            for (int i = 0; i < limit; i++)
                builder.Append(bitArray[i] ? 1 : 0);
            return builder.ToString();
        }

        public static void ParseStringCustom(this BitArray bitArray, string value)
        {
            int limit = value.Length;
            for (int i = 0; i < limit; i++)
                bitArray[i] = value[i] != '0';
        }

        public static List<string> SplitText(string text, int size)
        {
            var chunkSize = size;
            var stringLength = text.Length;
            var chunks = new List<string>();
            for (int i = 0; i < stringLength; i += chunkSize)
            {
                if (i + chunkSize > stringLength) chunkSize = stringLength - i;
                chunks.Add(text.Substring(i, chunkSize));
            }
            return chunks;
        } 
    }
}
