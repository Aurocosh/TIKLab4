﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Microsoft.Win32;

namespace TIKLab
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private HammingEngine _hamming;
        public MainWindow()
        {
            InitializeComponent();
            InitializeHamming(4);
        }

        private void InitializeHamming(int checkBitCount)
        {
            _hamming = new HammingEngine(32, checkBitCount);
        }

        private void ButtonGenerateCodes_OnClick(object sender, RoutedEventArgs e)
        {
            var text = TextBoxInput.Text;
            var symbolCount = _hamming.GenerateCodes(text);
            if (symbolCount != 0)
            {
                var message = 
                    "Не достаточно информационных битов чтобы вместить все символы." +
                    $"\nВ тексте присутствуют {symbolCount} разных символов, но {_hamming.InfoBitCount} битов способны " +
                    $"\nвместить тоьлко {_hamming.CalculateMaxSymbolCount(_hamming.InfoBitCount)} символов, .";
                MessageBox.Show(this, message);
            }

            var limit = _hamming.BitCount;

            var decCodes = _hamming.DecodingCodes.Select(v => ReplaceInvisible(v.Symbol) + " " + v.CodeBits.ToStringLimited(limit));
            TextBoxCodesRaw.Text = string.Join("\n", decCodes);
            var encCodes = _hamming.EncodingCodes.Select(v => ReplaceInvisible(v.Symbol) + " " + v.CodeBits.ToStringLimited(limit));
            TextBoxCodesControlled.Text = string.Join("\n", encCodes);
            ButtonEncode.IsEnabled = true;
        }

        private void ButtonEncode_OnClick(object sender, RoutedEventArgs e)
        {
            var text = TextBoxInput.Text;
            var codedText = _hamming.Encode(text);
            var limit = _hamming.BitCount;
            var resultCodes = codedText.Select(v => v.ToStringLimited(limit));
            var resultText = string.Join("", resultCodes);
            TextBoxBits.Text = resultText;
            TabItemDecode.IsSelected = true;
        }

        private void ButtonChaos_OnClick(object sender, RoutedEventArgs e)
        {
            float chaosProbablility;
            var chaosText = TextBoxChaosProbability.Text.Replace('.',',');

            bool parsed = float.TryParse(chaosText, out chaosProbablility);
            if (parsed)
            {
                var random = new Random();
                var builder = new StringBuilder(TextBoxBits.Text);
                int limit = builder.Length;
                for (int i = 0; i < limit; i++)
                {
                    if (random.NextDouble() < chaosProbablility)
                        builder[i] = builder[i] == '0' ? '1' : '0';
                }
                TextBoxBits.Text = builder.ToString();
            }
            else
            {
                var message = "Не верный формат вероятности";
                MessageBox.Show(this, message);
            }
        }

        private void ButtonDecode_OnClick(object sender, RoutedEventArgs e)
        {
            var overallBitCount = _hamming.OverallBitCount;
            var text = TextBoxBits.Text;
            var symbolTexts = Helper.SplitText(text, _hamming.BitCount);
            var symbols = new List<BitArray>();
            foreach (var symbolText in symbolTexts)
            {
                var bitArray = new BitArray(overallBitCount);
                bitArray.ParseStringCustom(symbolText);
                symbols.Add(bitArray);
            }
            TextBoxResult.Text = _hamming.Decode(symbols);
        }

        private void MenuItemOpen_OnClick(object sender, RoutedEventArgs e)
        {
            var openFileDialog = new OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
                TextBoxInput.Text = File.ReadAllText(openFileDialog.FileName);
            TabItemEncoding.IsSelected = true;
        }

        private void MenuItemClear_OnClick(object sender, RoutedEventArgs e)
        {
            Reset();
        }

        private void TextBoxCheckBitCount_OnTextChanged(object sender, TextChangedEventArgs e)
        {
            Console.Out.WriteLine(TextBoxCheckBitCount.Text);
            var checkBitCount = int.Parse(TextBoxCheckBitCount.Text);
            InitializeHamming(checkBitCount);
        }

        private void TextBoxCheckBitCount_OnPreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            e.Handled = !IsTextAllowed(e.Text);
        }

        private bool IsTextAllowed(string text)
        {
            var regex = new Regex("^[2-4]$"); 
            return regex.IsMatch(text);
        }

        private void Reset()
        {
            TextBoxInput.Text = "";
            TextBoxCodesControlled.Text = "";
            TextBoxCodesRaw.Text = "";
            TextBoxBits.Text = "";
            TextBoxResult.Text = "";
            TabItemEncoding.IsSelected = true;
            ButtonEncode.IsEnabled = true;
        }

        private string ReplaceInvisible(char c)
        {
            if(c == '\n') return "\\n";
            if(c == '\r') return "\\r";
            if(c == '\t') return "\\t";
            return c.ToString();
        }
    }
}
