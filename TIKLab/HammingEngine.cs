﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TIKLab
{
    public class HammingEngine
    {
        private readonly HammingEncoder _encoder;
        private readonly HammingDecoder _decoder;

        public List<Code> DecodingCodes { get; private set; }
        public List<Code> EncodingCodes { get; private set; }

        public int OverallBitCount { get; }
        public int BitCount { get; }
        public int CheckBitCount { get; }
        public int InfoBitCount => BitCount - CheckBitCount;
        public int CalculateBitCount(int checkBitCount) => (int)(Math.Pow(2, checkBitCount) - 1);
        public int CalculateMaxSymbolCount(int infoBitCount) => (int)Math.Pow(2, infoBitCount);

        public BitMaskGenerator MaskGenerator { get; }

        public HammingEngine(int overallBitCount, int checkBitCount)
        {
            _encoder = new HammingEncoder();
            _decoder = new HammingDecoder();
            OverallBitCount = overallBitCount;
            CheckBitCount = checkBitCount;
            BitCount = CalculateBitCount(checkBitCount);
            MaskGenerator = new BitMaskGenerator(overallBitCount);

            DecodingCodes = new List<Code>();
            EncodingCodes = new List<Code>();
        }

        public int GenerateCodes(string text)
        {
            var symbols = new HashSet<char>(text.ToCharArray());
            int maxSymbolCount = CalculateMaxSymbolCount(InfoBitCount);
            Console.Out.WriteLine("max symbol count " + maxSymbolCount);
            if (symbols.Count > maxSymbolCount)
            {
                return symbols.Count;
            }
            DecodingCodes = symbols.OrderBy(v=>v).Select((v, i) => new Code(v, new BitArray(BitConverter.GetBytes(i)))).ToList();
            _encoder.SetFields(BitCount, CheckBitCount, OverallBitCount);
            _encoder.SetMasks(MaskGenerator.BitMasks);
            EncodingCodes = DecodingCodes.Select(_encoder.AddAndFillControlBits).ToList();

            return 0;
        }

        public List<BitArray> Encode(string input)
        {
            var encodingDictionary = new Dictionary<char, BitArray>();
            foreach (var code in EncodingCodes)
                if (!encodingDictionary.ContainsKey(code.Symbol))
                    encodingDictionary.Add(code.Symbol, code.CodeBits);

            var encodedText = new List<BitArray>();
            foreach (var symbol in input)
            {
                BitArray array;
                bool codeExist = encodingDictionary.TryGetValue(symbol, out array);
                if (codeExist)
                    encodedText.Add(array);
                else
                {
                    var newArray = new BitArray(32);
                    newArray.SetAll(true);
                    encodedText.Add(newArray);
                }
            }

            return encodedText;
        }

        public string Decode(List<BitArray> input)
        {
            var decodingDictionary = new Dictionary<int, char>();
            foreach (var code in DecodingCodes)
            {
                var key = code.CodeBits.ToIntFromBitArray();
                if (!decodingDictionary.ContainsKey(key))
                    decodingDictionary.Add(key, code.Symbol);
            }


            _decoder.SetFields(BitCount, CheckBitCount, OverallBitCount);
            _decoder.SetMasks(MaskGenerator.BitMasks);
            var symbolBits = input.Select(_decoder.DecodeAndCheckSymbol).ToList();

            if (false)
            {
                Console.Out.WriteLine("masks");
                foreach (var bitArray in MaskGenerator.BitMasks)
                    Console.Out.WriteLine(bitArray.Key + " " + bitArray.Value.ToStringCustom());
                Console.Out.WriteLine("input");
                foreach (var bitArray in input)
                    Console.Out.WriteLine(bitArray.ToStringCustom());
                Console.Out.WriteLine("symbolBits");
                foreach (var bitArray in symbolBits)
                    Console.Out.WriteLine(bitArray.ToIntFromBitArray() + " " + bitArray.GetHashCode());
                Console.Out.WriteLine("dictionary");
                foreach (var pair in decodingDictionary)
                    Console.Out.WriteLine(pair.Value +" "+ pair.Key + " " + pair.Key.GetHashCode());
            }

            var chars = new List<char>();

            foreach (var array in symbolBits)
            {
                char symbol;
                bool valueExist = decodingDictionary.TryGetValue(array.ToIntFromBitArray(), out symbol);
                chars.Add(valueExist ? symbol : '?');
            }

            return string.Join("", chars);
        }
    }
}
