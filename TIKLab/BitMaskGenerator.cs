﻿using System.Collections;
using System.Collections.Generic;

namespace TIKLab
{
    public class BitMaskGenerator
    {
        public int MaskSize { get; }
        public Dictionary<int, BitArray> BitMasks { get; }

        public BitMaskGenerator(int maskSize)
        {
            MaskSize = maskSize;
            BitMasks = new Dictionary<int, BitArray>();
            for (int i = 1; i < maskSize; i *= 2)
                BitMasks[i] = GenerateBitMask(i-1, i);
        }

        public BitArray GenerateBitMask(int start, int step)
        {
            var mask = new BitArray(MaskSize);
            for (int i = start; i < MaskSize; i += step*2)
            {
                var limit = i + step;
                limit = limit > MaskSize ? MaskSize : limit;
                for (int j = i; j < limit; j++)
                    mask[j] = true;
            }
            return mask;
        }
    }
}
