﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TIKLab
{
    public class HammingDecoder
    {
        private bool _initialized;
        private int _bitCount;
        private int _checkBitCount;
        private int _overallBitCount;
        private Dictionary<int, BitArray> _masks;

        public HammingDecoder()
        {
            _initialized = false;
        }

        public void SetFields(int bitCount, int checkBitCount, int overallBitCount)
        {
            _bitCount = bitCount;
            _checkBitCount = checkBitCount;
            _overallBitCount = overallBitCount;
            _initialized = true;
        }

        public void SetMasks(Dictionary<int, BitArray> masks)
        {
            _masks = masks;
        }

        public BitArray DecodeAndCheckSymbol(BitArray codeBits)
        {
            if (!_initialized)
                throw new InvalidOperationException();
            var syndrome = new BitArray(_checkBitCount);

            for (int i = 0, power = 1; i < _checkBitCount; i++, power *= 2)
            {
                var mask = _masks[power];
                var controlledBits = new BitArray(codeBits);
                controlledBits.And(mask);
                var sum = controlledBits.Cast<bool>().Sum(bit => bit ? 1 : 0);
                var value = sum % 2 == 1;
                syndrome[i] = value;
            }

            var error = syndrome.Cast<bool>().Any(bit => bit);
            if (error)
            {
                int position = syndrome.ToIntFromBitArray() - 1;
                codeBits[position] = !codeBits[position];
            }
            var cleanCode = RemoveControlPositions(codeBits);
            return cleanCode;
        }

        private BitArray RemoveControlPositions(BitArray code)
        {
            if (!_initialized)
                throw new InvalidOperationException();
            var fromBit = 0;
            var powerOfTwo = 1;
            var newCode = new BitArray(_overallBitCount);
            for (int toBit = 0; toBit < _bitCount; fromBit++)
            {
                if (powerOfTwo - 1 == fromBit)
                    powerOfTwo *= 2;
                else
                    newCode[toBit++] = code[fromBit];
            }
            return newCode;
        }

    }
}
