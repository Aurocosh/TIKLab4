﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace TIKLab
{
    public class HammingEncoder
    {
        private bool _initialized;
        private int _bitCount;
        private int _checkBitCount;
        private int _overallBitCount;
        private Dictionary<int, BitArray> _masks;

        public HammingEncoder()
        {
            _initialized = false;
        }

        public void SetFields(int bitCount, int checkBitCount, int overallBitCount)
        {
            _bitCount = bitCount;
            _checkBitCount = checkBitCount;
            _overallBitCount = overallBitCount;
            _initialized = true;
        }

        public void SetMasks(Dictionary<int, BitArray> masks)
        {
            _masks = masks;
        }

        public Code AddAndFillControlBits(Code code)
        {
            if (!_initialized)
                throw new InvalidOperationException();

            var newCode = InsertControlPositions(code.CodeBits);

            for (int i = 0, power = 1; i < _checkBitCount; i++, power *= 2)
            {
                var mask = _masks[power];
                var controlledBits = new BitArray(newCode);
                controlledBits.And(mask);
                int sum = controlledBits.Cast<bool>().Sum(bit => bit ? 1 : 0);
                var value = sum % 2 == 1;
                newCode[power - 1] = value;
            }

            return new Code(code.Symbol, newCode);
        }

        public BitArray InsertControlPositions(BitArray code)
        {
            if (!_initialized)
                throw new InvalidOperationException();
            var fromBit = 0;
            var powerOfTwo = 1;
            var newCode = new BitArray(_overallBitCount);
            for (int toBit = 0; fromBit < _bitCount; toBit++)
            {
                if (powerOfTwo - 1 == toBit)
                    powerOfTwo *= 2;
                else
                    newCode[toBit] = code[fromBit++];
            }
            return newCode;
        }
    }
}
